#!/home/lordnino/.nvm/versions/node/v10.8.0/bin/node
const fs = require('fs')
const path = require('path')

class Store {

  constructor() {
    this.store_file = path.join(__dirname, "store.db")
    this.store_dictionary = JSON.parse(fs.readFileSync(this.store_file))
    this.available_methods = ['list', 'add', 'get', 'remove', 'clear']
    this.main()
    fs.writeFileSync(this.store_file, JSON.stringify(this.store_dictionary))
  }

  main() {
    if (this.store_file) {
      this.processArgv()
    } else {
      console.log(`Error: file not found`)
    }
  }

  processArgv() {
    this.method = process.argv[2]
    if (this.method) {
      switch (this.method) {
        case "list":
          this.list()
          break
        case "add":
          this.add()
          break
        case "remove":
          this.remove()
          break
        case "get":
          this.get()
          break
        case "clear":
          this.clear()
          break
        default:
          console.log(`Error: Unkown method`)
      }
    } else {
      console.log(`Error: Method can not be empty`)
    }
  }

  list() {
    console.log(this.store_dictionary)
  }

  add() {
    if (process.argv.length >= 5) {
      this.store_dictionary[process.argv[3]] = process.argv[4]
    } else {
      console.log(`Error: need key and value to add`)
    }
  }

  remove() {
    if (process.argv.length >= 3) {
      if(this.store_dictionary[process.argv[3]]) {
        delete(this.store_dictionary[process.argv[3]])
      } else {
        console.log(`Error: key not found`)
      }
    } else {
      console.log(`Error: need name of key`)
    }
  }

  get() {
    if (process.argv.length >= 3) {
      if(this.store_dictionary[process.argv[3]]) {
        console.log(this.store_dictionary[process.argv[3]])
      } else {
        console.log(`Error: key not found`)
      }
    } else {
      console.log(`Error: need name of key`)
    }
  }

  clear() {
    this.store_dictionary = {}
  }
}

store = new Store()
store